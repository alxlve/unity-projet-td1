﻿using UnityEngine;
using System.Collections;

public class showBackground : MonoBehaviour {

    public bool isMirrorBackground;
    public bool pause;
    public Vector2 velocity;
    GameObject background0;
    GameObject background1;
    GameObject background2;
    GameObject background3;

	void Start () {
        background0 = Instantiate(Resources.Load("background")) as GameObject;
        background1 = Instantiate(Resources.Load("background")) as GameObject;
        background2 = Instantiate(Resources.Load("background")) as GameObject;
        background3 = Instantiate(Resources.Load("background")) as GameObject;
        background0.GetComponent<positionBackground>().isMirrorBackground = isMirrorBackground;
        background1.GetComponent<positionBackground>().isMirrorBackground = isMirrorBackground;
        background2.GetComponent<positionBackground>().isMirrorBackground = isMirrorBackground;
        background3.GetComponent<positionBackground>().isMirrorBackground = isMirrorBackground;
        background0.GetComponent<positionBackground>().backgroundNumber = 0;
        background1.GetComponent<positionBackground>().backgroundNumber = 1;
        background2.GetComponent<positionBackground>().backgroundNumber = 2;
        background3.GetComponent<positionBackground>().backgroundNumber = 3;
	}

    void Update() {
        if (!pause) {
            background0.GetComponent<moveBackground>().velocity = velocity;
            background1.GetComponent<moveBackground>().velocity = velocity;
            background2.GetComponent<moveBackground>().velocity = velocity;
            background3.GetComponent<moveBackground>().velocity = velocity;
        } else {
            background0.GetComponent<moveBackground>().velocity = new Vector2(0, 0);
            background1.GetComponent<moveBackground>().velocity = new Vector2(0, 0);
            background2.GetComponent<moveBackground>().velocity = new Vector2(0, 0);
            background3.GetComponent<moveBackground>().velocity = new Vector2(0, 0);
        }
    }

    public void resetPosition(bool init) {
        background0.GetComponent<positionBackground>().resetPosition(init);
        background1.GetComponent<positionBackground>().resetPosition(init);
        background2.GetComponent<positionBackground>().resetPosition(init);
        background3.GetComponent<positionBackground>().resetPosition(init);
    }

}
