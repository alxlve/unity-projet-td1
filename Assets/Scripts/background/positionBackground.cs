﻿using UnityEngine;
using System.Collections;

/* Need four background, because of the isMirrorBackground option.
 * 1212 -> 2121 -> ok.
 * 121 -> 211 -> problem.
 */
public class positionBackground : MonoBehaviour {

    /* 
     * isMirrorBackground == true = a mirror image is used.
     * Make it easy to use backgrounds found on the web,
     * just mirror a picture on a photo editing software,
     * and tick isMirrorBackground.
     * Otherwise just don't tick this option.
     */
    public bool isMirrorBackground;
    public int backgroundNumber;
    private int backgroundPosition;
    private float worldScreenHeight;
    private float worldScreenWidth;
    private Vector3 leftBottomCameraBorder;
    private Vector3 size;

    void Start () {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        worldScreenHeight = Camera.main.orthographicSize * 2;
        worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        backgroundPosition = backgroundNumber;

        resetPosition(true);
	}
	
    void Update () {
        if (!isMirrorBackground) {
            // View the entire background in the camera.
            transform.localScale = new Vector3(worldScreenWidth / size.x, worldScreenHeight / size.y, 1);
            if (transform.position.x <= leftBottomCameraBorder.x - (worldScreenWidth / 2)) {
                GameObject background = GameObject.FindGameObjectWithTag("background");
                background.GetComponent<showBackground>().pause = true;
                background.GetComponent<showBackground>().resetPosition(false);
                background.GetComponent<showBackground>().pause = false;
            }
        } else {
            // View the entire background in the camera / 2 for x axis.
            transform.localScale = new Vector3(worldScreenWidth / (size.x / 2), worldScreenHeight / size.y, 1);
            if (transform.position.x <= leftBottomCameraBorder.x - worldScreenWidth) {
                GameObject background = GameObject.FindGameObjectWithTag("background");
                background.GetComponent<showBackground>().pause = true;
                background.GetComponent<showBackground>().resetPosition(false);
                background.GetComponent<showBackground>().pause = false;
            }
        }
	}

    /*
     * We reset each position, whenever we move manually a background.
     * It permits the use of a really high velocity value,
     * because it avoids a blank space between two background images.
     */
    public void resetPosition(bool init) {
        if (!init) {
            backgroundPosition--;
            if (backgroundPosition < 0) {
                backgroundPosition = 3;
            }
        }
        if (!isMirrorBackground) {
            // Set the initial padding, x = 0, center the image.
            transform.position = new Vector3(0 + (worldScreenWidth * backgroundPosition), transform.position.y, transform.position.z);
        } else {
            // Set the initial padding, x = -leftBottomCameraBorder.x, show the first part of the mirror.
            transform.position = new Vector3(-leftBottomCameraBorder.x + (worldScreenWidth * backgroundPosition * 2), transform.position.y, transform.position.z);
        }
    }

}
