﻿using UnityEngine;
using System.Collections;

public class fadeOut : MonoBehaviour {

    public float fade = 0.05f;
	
	void Update() {
        Color color = GetComponent<SpriteRenderer>().color;
        color.a -= fade;
        GetComponent<SpriteRenderer>().color = color;
        if (color.a < 0) {
            Destroy(gameObject);
        }
	}

}
