﻿using UnityEngine;
using System.Collections;

public class launchGame : MonoBehaviour {
	
	void Update () {
        bool load = false;
        int nTouch = Input.touchCount;

        if (nTouch > 0) {
            for (int touch = 0; touch < nTouch; touch++) {
                if ((Input.GetTouch(touch).phase != TouchPhase.Ended) && (Input.GetTouch(touch).phase != TouchPhase.Canceled)) {
                    load = true;
                }
            }
        }

        if ((Input.GetKey(KeyCode.Space) || load)) {
            Destroy(GameObject.FindWithTag("MainCamera").GetComponent<launchGame>());
            Application.LoadLevel("scene1");
            GameObject.FindWithTag("MainCamera").GetComponent<GameState>().resetScorePlayer();
        }
	}

}
