﻿using UnityEngine;
using System.Collections;

public class detectIfAsteroid : MonoBehaviour {

    public int maxAsteroid;
    private Vector3 rightBottomCameraBorder;
    private Vector3 size;

	void Start () {
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
	}
	
	void Update () {
        int score;
        int threshold;
        float random;

        GameObject[] respawns = GameObject.FindGameObjectsWithTag("asteroid");
        if (respawns.Length > 0) {
            size.x = respawns[0].GetComponent<SpriteRenderer>().bounds.size.x;
            size.y = respawns[0].GetComponent<SpriteRenderer>().bounds.size.y;
        }

        if (respawns.Length < maxAsteroid) {
            // Add some difficulty over time...
            score = GameObject.FindWithTag("MainCamera").GetComponent<GameState>().getScorePlayer();
            threshold = score / 5;

            if ((Random.Range(0, 100) <= threshold) || (respawns.Length < 4)) {
                Vector3 position = new Vector3(rightBottomCameraBorder.x + (size.x / 2), Random.Range(rightBottomCameraBorder.y + (size.y / 2), transform.position.z));
                GameObject asteroid = Instantiate(Resources.Load("asteroid"), position, Quaternion.identity) as GameObject;
                
                random = Random.Range(asteroid.GetComponent<moveAsteroid>().minScale, asteroid.GetComponent<moveAsteroid>().maxScale);
                asteroid.transform.localScale = new Vector3(asteroid.transform.localScale.x + random, asteroid.transform.localScale.y + random, asteroid.transform.localScale.z + random);
                
                random = Random.Range(asteroid.GetComponent<moveAsteroid>().minXVelocity, asteroid.GetComponent<moveAsteroid>().maxXVelocity);
                asteroid.GetComponent<moveAsteroid>().velocity = new Vector2(random, 0);
            }
        }
	}
}
