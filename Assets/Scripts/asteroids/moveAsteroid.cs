﻿using UnityEngine;
using System.Collections;

public class moveAsteroid : MonoBehaviour {

    public float angle;
    public float minScale;
    public float maxScale;
    public float minXVelocity;
    public float maxXVelocity;
    public Vector2 velocity;
    private Vector3 leftBottomCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 size;

    void Start() {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
      }

    void Update() {
        GetComponent<Rigidbody2D>().velocity = velocity;

        // Rotation automatisée du sprite.
        transform.rotation = Quaternion.AngleAxis(angle++, Vector3.forward);

        /*
         * Calcul de la taille du sprite auquel ce script est attaché :
         * Taille "normale" = gameObject.GetComponent<SpriteRenderer>().bounds.size;
         * On prend en compte les éventuelles transformations demandées lors de l'intégration du sprite dans
         * 'size' vaut alors la taille normale *  par les déformations (notamment le zoom).
         */
        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        /*
         * x = centre + rayon * sin(degre1) * cos(x²)
         * y = centre + rayon * cos(degre2)
         * x = centre + rayon * sin(degre1) * cos(x²)
         * y = centre + rayon * cos(degre1) // Dessine un cercle.
         * x = centre + rayon * sin(degre1) * cos(x²)
         * y = centre + rayon * cos(degre1 * 2) // Dessine un 8.
         */

        //if (transform.position.x + (size.x / 2) < leftBottomCameraBorder.x) {
        //    gameObject.transform.position = new Vector3(rightBottomCameraBorder.x, Random.Range(rightBottomCameraBorder.y, rightTopCameraBorder.y), transform.position.z);
        //}
        if (transform.position.x + (size.x / 2) < leftBottomCameraBorder.x) {
            Vector3 newPosition = new Vector3(rightBottomCameraBorder.x, Random.Range(rightBottomCameraBorder.y, rightTopCameraBorder.y), transform.position.z);
            GameObject next = Instantiate(Resources.Load("asteroid"), newPosition, Quaternion.identity) as GameObject;
            next.GetComponent<moveAsteroid>().angle = gameObject.GetComponent<moveAsteroid>().angle;
            next.GetComponent<moveAsteroid>().velocity = gameObject.GetComponent<moveAsteroid>().velocity;
            Destroy(gameObject);
        }
    }

}
