﻿using UnityEngine;
using System.Collections;

public class positionShip : MonoBehaviour {

    private Vector3 leftBottomCameraBorder;
    private Vector3 leftTopCameraBorder;
    private Vector3 size;

	void Start() {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        
        transform.position = new Vector3(leftBottomCameraBorder.x + (size.x / 2), transform.position.y, transform.position.z);
    }
	
	void Update() {
        if (transform.position.y - (size.y / 2) < leftBottomCameraBorder.y) {
            transform.position = new Vector3(transform.position.x, leftBottomCameraBorder.y + (size.y / 2), transform.position.z);
        }
        if (transform.position.y + (size.y / 2) > leftTopCameraBorder.y) {
            transform.position = new Vector3(transform.position.x, leftTopCameraBorder.y - (size.y / 2), transform.position.z);
        }
	}

}
