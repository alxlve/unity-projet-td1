﻿using UnityEngine;
using System.Collections;

public class shootAgain : MonoBehaviour {

    public float shotsPerSecond;
    private float timeBetweenShots;
    public Vector2 velocity;
    private float timestamp;
    private Vector3 size;

    void Start() {
        timeBetweenShots = 1 / shotsPerSecond;
    }

	void Update() {
        bool space = false;
        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        int nTouch = Input.touchCount;

        if (nTouch > 0) {
            for (int touch = 0; touch < nTouch; touch++) {
                if ((Input.GetTouch(touch).phase != TouchPhase.Ended) && (Input.GetTouch(touch).phase != TouchPhase.Canceled)) {
                    space = true;
                }
            }
        }

        if (Time.time >= timestamp && (Input.GetKey(KeyCode.Space) || space)) {
            Vector3 position = new Vector3(transform.position.x + (size.x / 2), transform.position.y, transform.position.z);
            GameObject shoot = Instantiate(Resources.Load("shootLaserViolet"), position, Quaternion.identity) as GameObject;
            shoot.GetComponent<moveShoot>().velocity = velocity;
            GameObject.FindWithTag("MainCamera").GetComponent<SoundState>().playShotSound();
            timestamp = Time.time + timeBetweenShots;
        }
	}

}
