﻿using UnityEngine;
using System.Collections;

//#pragma warning disable CS0414
public class moveShoot : MonoBehaviour {

    public Vector2 velocity;
    private bool isEnemyShot;
    private Vector3 rightBottomCameraBorder;

    private Vector3 size;

	void Start() {
        isEnemyShot = false;
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
	}
	
	void Update() {
        GetComponent<Rigidbody2D>().velocity = velocity;
        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        if (transform.position.x - (size.x) > rightBottomCameraBorder.x) {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        // We try not to shoot ourselves.
        if (!isEnemyShot && (other.gameObject.tag != "player")) {
            Destroy(other.gameObject.GetComponent<BoxCollider2D>());
            other.gameObject.AddComponent<fadeOut>();
            GameState.instance.addScorePlayer(1);
            Destroy(gameObject);
        }
    }
    
}
