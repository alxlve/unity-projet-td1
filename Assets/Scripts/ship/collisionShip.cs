﻿using UnityEngine;
using System.Collections;

public class collisionShip : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other) {
        //Debug.Log(GetComponent<Collider2D>().name);
        //Debug.Log(other.name);
        //Debug.Log(other.gameObject.tag);
        if (other.gameObject.tag == "asteroid") {
            if (GameObject.FindGameObjectWithTag("life5")) {
                GameObject.FindGameObjectWithTag("life5").AddComponent<fadeOut>();
            } else if (GameObject.FindGameObjectWithTag("life4")) {
                GameObject.FindGameObjectWithTag("life4").AddComponent<fadeOut>();
            } else if (GameObject.FindGameObjectWithTag("life3")) {
                GameObject.FindGameObjectWithTag("life3").AddComponent<fadeOut>();
            } else if (GameObject.FindGameObjectWithTag("life2")) {
                GameObject.FindGameObjectWithTag("life2").AddComponent<fadeOut>();
            } else if (GameObject.FindGameObjectWithTag("life1")) {
                GameObject.FindGameObjectWithTag("life1").AddComponent<fadeOut>();
                Destroy(GameObject.FindGameObjectWithTag("player").GetComponent<shootAgain>());
                GameObject.FindGameObjectWithTag("player").AddComponent<fadeOut>();
                GameObject.FindWithTag("MainCamera").GetComponent<SoundState>().playDeathSound();
                GameObject.FindGameObjectWithTag("MainCamera").AddComponent<launchTitle>();
            }
        }
    }

}
