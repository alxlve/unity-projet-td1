﻿using UnityEngine;
using System.Collections;

public class moveShip : MonoBehaviour {

    public float velocity;
    public float velocitySmartphone;
	
    void Update() {
        float translation;
        float translationSmartphone;

        translation = Input.GetAxis("Vertical") * velocity;
        translation *= Time.deltaTime;
        transform.Translate(0, translation, 0);

        translationSmartphone = -Input.acceleration.x * velocity * Time.deltaTime;
        transform.Translate(0, translationSmartphone, 0);

        /*
         * Move in every direction.
         */ 
        //if (Input.GetKey(KeyCode.UpArrow)) {
        //    transform.position += Vector3.up * velocity * Time.deltaTime;
        //}
        //if (Input.GetKey(KeyCode.RightArrow)) {
        //    transform.position += Vector3.right * velocity * Time.deltaTime;
        //}
        //if (Input.GetKey(KeyCode.DownArrow)) {
        //    transform.position += Vector3.down * velocity * Time.deltaTime;
        //}
        //if (Input.GetKey(KeyCode.LeftArrow)) {
        //    transform.position += Vector3.left * velocity * Time.deltaTime;
        //}
    }

}
