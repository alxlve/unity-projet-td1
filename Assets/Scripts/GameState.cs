﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour {

    public static GameState instance;
    private int scoreDigits;
    private int scorePlayer;

	void Start() {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(instance.gameObject);
            /*
             * Define the number of digits to show by default,
             * if you set Text of scoreText to "000",
             * it will show at least 3 digits by example.
             */
            scoreDigits = GameObject.FindWithTag("scoreText").GetComponent<GUIText>().text.Length;
        } else if (this != instance) {
            Destroy(this.gameObject);
        }
	}

    void FixedUpdate() {
        GameObject.FindWithTag("scoreText").GetComponent<GUIText>().text = scorePlayer.ToString("D" + scoreDigits);
    }

    public void addScorePlayer(int score) {
        scorePlayer += score;
    }

    public void resetScorePlayer() {
        scorePlayer = 0;
    }

    public int getScorePlayer() {
        return scorePlayer;
    }

}
