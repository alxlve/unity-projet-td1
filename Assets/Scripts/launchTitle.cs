﻿using UnityEngine;
using System.Collections;

public class launchTitle : MonoBehaviour {

	void Update () {
        bool load = false;
        int nTouch = Input.touchCount;

        if (nTouch > 0) {
            for (int touch = 0; touch < nTouch; touch++) {
                if ((Input.GetTouch(touch).phase != TouchPhase.Ended) && (Input.GetTouch(touch).phase != TouchPhase.Canceled)) {
                    load = true;
                }
            }
        }

        if ((Input.GetKey(KeyCode.Space) || load)) {
            Destroy(GameObject.FindWithTag("MainCamera").GetComponent<launchTitle>());
            Application.LoadLevel("title");
        }
	}

}
