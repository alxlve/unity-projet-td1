﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundState : MonoBehaviour {

    public static SoundState instance;
    public AudioClip playerDeathSound;
    public AudioClip playerShotSound;

	void Start () {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(instance.gameObject);
        } else if (this != instance) {
            Destroy(this.gameObject);
        }
	}

    public void playDeathSound() {
        MakeSound(playerDeathSound);
    }

    public void playShotSound() {
        MakeSound(playerShotSound);
    }

    private void MakeSound(AudioClip originalClip) {
        AudioSource.PlayClipAtPoint(originalClip, transform.position);
    }

}
