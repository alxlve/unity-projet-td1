Alexis Lavie

demo/ : contient les ex�cutables Web + Android.


Quelques notes br�ves sur ce qui a �t� fait :

- S'adapte � diff�rentes r�solutions.
- Int�gre du code al�atoire pour les ast�roides.
- Les scripts ont pour la plupart des attributs de configuration accessibles
  dans l'�diteur Unity.
- Fond d�filant (tr�s complet, gestion des v�locit�s rapides, voir code).
- Commentaires (il y en a mais tr�s peu, seulement o� je pense que
  c'est n�cessaire, comme pour le fond d�filant)...
  Ce qui est "mainstream" je ne l'ai pas comment�.
- Support d'Android (test� sur tablette 1280x720).
- Une sc�ne qui combine d�but / fin, + la sc�ne de jeu.
- Graphiquement j'ai quasi tout modifi�, avec des images en haute d�finition,
  certaines sont modifi�es, ou cr�es par mes soins.
  (Voir l'organisation des dossiers... dans FilesResources).
  -> visuellement c'est propre et plus joli.
- Les sons ont un bitrate �lev�...
- Utilise l'univers Star Wars pour le fun.

(Autrement 'git log').


Ce que je peux am�liorer :

- Mettre des ennemis, les sprites sont d�j� trouv�s.
- Am�liorer les sc�nes de d�but et fin.
- Choix de vaisseau.
- Encore un peu plus d'al�atoire (sons, couleurs du laser, images d'ast�ro�des).
- Plus d'effets...


Je n'ai pas cr�� d'ex�cutable pour Windows ou Mac ou Linux,
car il suffit de build sans configuration particuli�re (le d�p�t est d�j� assez gros).
